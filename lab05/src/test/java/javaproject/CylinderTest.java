// Johakim Fontaine Barboza
package javaproject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CylinderTest {
    
    @Test
    public void constructerTest() 
    {
        SkeletonCylinder cylinder = new SkeletonCylinder(2, 3);
        assertEquals(2, cylinder.getRadius(), 0);
        assertEquals(3, cylinder.getLength(),0);

    }
    @Test
    public void getVolumeTest_returns377()
    {
        SkeletonCylinder cylinder = new SkeletonCylinder(2, 3);
        assertEquals("Checks if both volumes are equal", 37.7, cylinder.getVolume(), 0.01);
    }
    @Test
    public void getSurfaceAreaTest()
    {
        SkeletonCylinder cylinder = new SkeletonCylinder(2, 3);
        assertEquals("Checks if both surface areas are equal", 62.83, cylinder.getSurfaceArea(), 0.01);
    }
}
