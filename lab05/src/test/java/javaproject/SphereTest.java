//Yun Chen Qian
package javaproject;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SphereTest {

    @Test
    public void constructorTest(){
        SkeletonSphere s = new SkeletonSphere(4);
    }

    @Test
    public void getVolumeTest(){
        SkeletonSphere s = new SkeletonSphere(4);
        assertEquals(268.08, s.getVolume(), 0.01);
    }

    @Test
    public void getSurfaceAreaTest(){
        SkeletonSphere s = new SkeletonSphere(4);
        assertEquals(201.06, s.getSurfaceArea(), 0.1); 
    }
}
