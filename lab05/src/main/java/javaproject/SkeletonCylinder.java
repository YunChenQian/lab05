// Yun Chen Qian

package javaproject;

public class SkeletonCylinder implements  Shape3d {
    private double radius;
    private double length;
    /**
     * 
     * @param radius
     * @param length
     */
    public SkeletonCylinder(double radius, double length){
        this.radius = radius;
        this.length = length;
    }

    /**
     * @return return radius
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * @return returns length
     */
    public double getLength(){
        return this.length;
    }
    /** 
     * @return double returns the volume
     */
    public double getVolume(){
        return  Math.PI * this.radius * this.radius * this.length;
    }

    /**
     * @return double returns the surface area
     */
    public double getSurfaceArea(){
        return 2*Math.PI * this.radius * this.radius + 2 * Math.PI * this.radius * this.length;
    }
}
