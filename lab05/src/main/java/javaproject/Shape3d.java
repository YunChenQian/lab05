package javaproject;

public interface Shape3d {
    double getVolume();
    double getSurfaceArea();
}
