// Johakim Fontaine Barboza
package javaproject;

public class SkeletonSphere implements Shape3d {
    private double radius;

    /**
     * Constructor for the Sphere class
     * @param radius
     */
    public SkeletonSphere(double radius){
        this.radius = radius;
    }
    /**
     * 
     * @return The radius of the sphere
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * @return The volume of the sphere
     */
    public double getVolume() {
        return (4.0/3.0 * Math.PI * Math.pow(this.radius, 3));
    }
    /**
     * @return The surface area of the fear
     */
    public double getSurfaceArea() {
        return 4 * Math.PI * Math.pow(this.radius, 2);
    }
}